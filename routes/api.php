<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('auth')->group(function () {
    Route::post('/', [AuthController::class, 'login']);
    //уточнить про запись /check-token
    Route::get('/check-token', [AuthController::class, 'checkToken']);
    Route::get('/logout', [AuthController::class, 'logout']);
});

Route::prefix('adminPanel')->middleware(['check', 'access:1'])->group(function () {
    Route::delete('/deleteUser/{id}', [EmployeeController::class, 'destroy']);
    Route::post('/addUser', [EmployeeController::class, 'store']);
    Route::put('/editUser/{id}', [EmployeeController::class, 'edit']);

    Route::delete('/deleteCompany/{id}', [CompanyController::class, 'destroy']);
    Route::post('/addCompany', [CompanyController::class, 'store']);
    Route::put('/editCompany/{id}', [CompanyController::class, 'update']);
});

Route::prefix('adminPanel')->group(function () {
    Route::apiResource('employees', EmployeeController::class);
    Route::apiResource('companies', CompanyController::class);
});

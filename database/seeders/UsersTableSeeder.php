<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'login' => 'manager',
                'password' => Hash::make('!A12fvcs'),
                'role_id' => 1
            ],
            [
                'login' => 'admin',
                'password' => Hash::make('!A12fvcs2'),
                'role_id' => 2
            ]
        ];

        foreach ($users as $user) {
            User::create($user);
        }
    }

}

<?php

namespace Database\Seeders;

use App\Models\Company;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,500) as $index) {
            $company = $faker->company;
            $address = $faker->address;
            $website = $faker->url;
            $email = $faker->unique()->safeEmail;
            $logo = $faker->imageUrl(100, 100, 'business');

            DB::table('companies')->insert([
                'company' => $company,
                'address' => $address,
                'website' => $website,
                'email' => $email,
                'logo' => $logo,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}

import Vue from 'vue';
import Vuelidate from 'vuelidate';
import router from './router/router'; // assuming router.js is in the same directory
import store from './store/index';
import App from './components/App';
import 'bootstrap/dist/css/bootstrap.css'

import 'admin-lte/dist/css/adminlte.min.css';
import 'admin-lte/plugins/fontawesome-free/css/all.min.css'
import 'admin-lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css'
import 'admin-lte/dist/css/adminlte.min.css?v=3.2.0'
// import './style.scss'

import 'admin-lte/plugins/jquery/jquery.min.js'
import 'admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js'
import 'admin-lte/dist/js/adminlte.min.js?v=3.2.0'


Vue.use(Vuelidate);  // здесь происходит регистрация Vuelidate

// Mount Vue application
if (document.querySelector('#app')) {
    new Vue({
        router,
        store,
        render: h => h(App),
    }).$mount('#app');
}

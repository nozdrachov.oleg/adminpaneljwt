import axios from "axios";

const companies = {
    namespaced: true,
    state: {
        users: {},
        pagination: {},
        currentPage: 1,
        lastPage: 0
    },
    getters: {
        users: state => state.users,
        currentPage: state => state.currentPage,
        lastPage: state => state.lastPage,
    },
    mutations: {
        setUsers(state, { data, ...pagination }) {
            state.users = data;
            state.pagination = pagination;
        },
        setCurrentPage(state, page) {
            state.currentPage = page;
        },
        setLastPage(state, lastPage) {
            state.lastPage = lastPage;
        },
        setDeleteUser(state, id) {
            state.users = state.users.filter(t => t.id !== id);
        },
        setEditUser(state, updatedUser) {
            const index = state.users.findIndex(u => u.id === updatedUser.id);
            if (index !== -1) {
                state.users.splice(index, 1, updatedUser);
            } else {
                console.log('User with ID not found in state.users');
            }
        }
    },
    actions: {
        async showUsers({ commit, state }, { page = state.currentPage, typePage }) {
            try {
                const response = await axios.get(`/api/adminPanel/${typePage}?page=${page}`);
                if (response.status === 200 && response.data.users) {
                    commit('setUsers', response.data.users);
                    // commit('setCurrentPage', response.data.users.current_page);
                    commit('setLastPage', response.data.users.last_page);
                } else {
                    console.log("не получили данные с сервера");
                }
            } catch (error) {
                console.log(error);
            }
        },
        async deleteUser({commit}, id) {
            try {
                const response = await axios.delete(`/api/adminPanel/deleteCompany/${id}`);
                if (response.status === 204) {
                    commit('setDeleteUser', id);
                }
            } catch (error) {
                console.log(error);
            }
        },
        async editUser({ commit }, userData) {
            try {
                const response = await axios.put(`/api/adminPanel/editCompany/${userData.id}`, userData);
                if (response.status === 201) {
                    commit('setEditUser', response.data.users);
                }
            } catch (error) {
                console.log(error);
            }
        },
        async addUser({commit}, user) {
            try {
                const response = await axios.post(`/api/adminPanel/addCompany`, user);
                if (response.status === 201 && response.data.users) {
                    commit('setUsers', response.data.users);
                    commit('setLastPage', response.data.users.last_page);
                }
            } catch (error) {
                console.log(error);
            }
        }
    }
}

export default companies;

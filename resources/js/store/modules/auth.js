import axios from 'axios';

export default {
    namespaced: true,
    state: {
        lvl: null,
        auth: false,
        error: {},
        tokenChecked: false
    },
    getters: {
        error: state => state.error,
        isAuthenticated: state => state.auth,
        lvl: state => state.lvl
    },
    mutations: {
        setLvl(state, lvl) {
            state.lvl = lvl;
        },
        setTokenChecked(state, tokenChecked) {
            state.tokenChecked = tokenChecked;
        },
        setAuth(state, auth) {
            state.auth = auth;
        },
        setError(state, errors) {
            state.error = errors;
        }
    },
    actions: {
        async getAuth({commit}, credentials) {
            try {
                const response = await axios.post('/api/auth', credentials);
                if (response.data.lvl && response.data.auth) {
                    commit('setLvl', response.data.lvl);
                    commit('setAuth', response.data.auth);
                }
            }
            catch (error) {
                if (error.response && error.response.status === 422) {
                    commit('setError', error.response.data.errors);
                }
                throw error;
            }
        },
        async checkToken({commit}) {
            try {
                const response = await axios.get('/api/auth/check-token');
                if (response.data.valid) {
                    commit('setLvl', response.data.lvl);
                    commit('setAuth', true);
                    return true;
                } else {
                    commit('setAuth', false);
                    return false; // Возвращаем false, если токен не валиден
                }
            }
            catch (error) {
                commit('setAuth', false);
                console.error(error); // Записываем ошибку в консоль
                return false; // Возвращаем false, если произошла ошибка
            }
        },
        async logout({commit}) {
            try {
                await axios.get('/api/auth/logout');
                commit('setAuth', false);
                commit('setLvl', null);
            }
            catch (error) {
                console.log('Ошибка на сервере , очистите кеш или выйдите позже');
            }
        }
    }
}

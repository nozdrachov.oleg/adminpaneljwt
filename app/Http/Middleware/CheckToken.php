<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use PHPOpenSourceSaver\JWTAuth\Exceptions\JWTException;
use PHPOpenSourceSaver\JWTAuth\Exceptions\TokenExpiredException;
use PHPOpenSourceSaver\JWTAuth\Exceptions\TokenInvalidException;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth;

class CheckToken
{
    public function handle(Request $request, Closure $next)
    {
        try {
            $token = $request->cookie('jwt_access');
            $isValid = JWTAuth::setToken($token)->check();

            if (!$isValid) {
                $token = JWTAuth::refresh($token);
                $request->cookies->set('jwt_access', $token);

                $newCookie = cookie('jwt_access', $token, 1440, null, null, false, true);
                return $next($request)->withCookie($newCookie);
            }

        } catch (TokenExpiredException $e) {
            Log::error('Token Expired: ' . $e->getMessage());
            return response()->json(['valid' => false], 401);

        } catch (TokenInvalidException $e) {
            Log::error('Token Invalid: ' . $e->getMessage());
            return response()->json(['valid' => false], 401);

        } catch (JWTException $e) {
            Log::error('JWT Exception: ' . $e->getMessage());
            return response()->json(['valid' => false], 401);
        }

        return $next($request);
    }
}

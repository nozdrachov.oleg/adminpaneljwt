<?php

namespace App\Contracts;

interface CompanyServiceInterface
{
    public function getAllEmployees();
    public function createEmployee(array $data);
    public function deleteEmployee($id);
    public function editEmployee($id, $data);
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Contracts\EmployeeServiceInterface;
use App\Services\EmployeeService;
use App\Contracts\CompanyServiceInterface;
use App\Services\CompanyService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Регистрация EmployeeService
        $this->app->bind(EmployeeServiceInterface::class, function ($app) {
            return new EmployeeService();
        });

        // Регистрация CompanyService
        $this->app->bind(CompanyServiceInterface::class, function ($app) {
            return new CompanyService();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

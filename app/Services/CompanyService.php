<?php

namespace App\Services;

use App\Contracts\CompanyServiceInterface;
use App\Models\Company;

class CompanyService implements CompanyServiceInterface
{
    public function getAllEmployees()
    {
        return Company::orderBy('id', 'desc')->paginate(15);
    }

    public function createEmployee(array $data)
    {
        Company::create($data);
        $users = Company::orderBy('id', 'desc')->paginate(15);

        if ($users->isEmpty()) {
            return response()->json(null, 401);
        }

        return $users;
    }

    public function editEmployee($id, $data)
    {
        $company = Company::find($id);

        if ($company) {
            // Обновляем данные компании
            $company->update($data);

            // Возвращаем обновленные данные компании
            return $company->fresh();
        }

        return null; // Возвращаем null, если компания не найдена
    }

    public function deleteEmployee($id)
    {
        Company::findOrFail($id)->delete();
    }
}

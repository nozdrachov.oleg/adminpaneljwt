<?php

namespace App\Services;

use App\Contracts\EmployeeServiceInterface;
use App\Models\Company;
use App\Models\Employee;
use Illuminate\Support\Facades\DB;

class EmployeeService implements EmployeeServiceInterface
{
    public function getAllEmployees()
    {
        return Employee::with('company')->orderBy('id', 'desc')->paginate(15);
    }

    public function createEmployee($data)
    {
        // Извлекаем имя компании и удаляем его из массива данных
        $companyName = $data['company'];
        unset($data['company']);

        DB::beginTransaction();

        try {
            // Создаем запись компании в базе данных
            $company = Company::create(['company' => $companyName]);

            // Добавляем ID компании к данным сотрудника
            $data['company_id'] = $company->id;

            // Создаем запись сотрудника
            $employee = Employee::create($data);

            DB::commit();


            return $this->getAllEmployees();
        } catch (\Exception $e) {
            DB::rollback(); // В случае исключения откатываем транзакцию
            // Возвращаем сообщение об ошибке
            return ['error' => $e->getMessage()];
        }
    }

    public function editEmployee($id, $data)
    {
        $user = Employee::with('company')->find($id);

        if ($user) {
            // Обновляем данные сотрудника
            $user->update($data);

            // Если есть данные для обновления компании, обновляем компанию
            if (isset($data['company']) && isset($data['company']['company'])) {
                $user->company->update(['company' => $data['company']['company']]);
            }

            // Возвращаем обновленные данные сотрудника с компанией
            return Employee::with('company')->find($id);
        }

        return null; // Возвращаем null, если пользователь не найден
    }



    public function deleteEmployee($id)
    {
        Employee::findOrFail($id)->delete();
    }
}
